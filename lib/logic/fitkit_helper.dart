import 'package:fit_kit/fit_kit.dart';

class FitKitHelper {
  static getEnergy({int days}) async {
    return await FitKit.read(
      DataType.ENERGY,
      DateTime.now().subtract(Duration(days: days)),
      DateTime.now(),
    );
  }

  static getStepCount({int days}) async {
    return await FitKit.read(
      DataType.STEP_COUNT,
      DateTime.now().subtract(Duration(days: days)),
      DateTime.now(),
    );
  }
  //there's other types of data too
}
