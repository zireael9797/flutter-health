import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:flutter_health/logic/fitkit_helper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeScreen extends StatefulWidget {
  static final String id = "/";

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey<AnimatedCircularChartState> _chartKey =
      GlobalKey<AnimatedCircularChartState>();

  void readEnergy() async {
    //collection of data over the last 5 days
    final stepResults = await FitKitHelper.getStepCount(days: 5);
    final energyResults = await FitKitHelper.getEnergy(days: 5);

    List<CircularStackEntry> nextData = <CircularStackEntry>[
      CircularStackEntry(
        <CircularSegmentEntry>[
          //only taking the first of the many results
          CircularSegmentEntry(energyResults[1].value.toDouble(), Colors.blue,
              rankKey: 'Q1'),
          CircularSegmentEntry(2000 - energyResults[1].value.toDouble(),
              Colors.red, //out of 2000 calories target
              rankKey: 'Q2'),
        ],
      ),
      CircularStackEntry(
        <CircularSegmentEntry>[
          //only taking the fifth result
          CircularSegmentEntry(stepResults[5].value.toDouble(), Colors.green,
              rankKey: 'Q1'),
          CircularSegmentEntry(200 - stepResults[5].value.toDouble(),
              Colors.red, //out of 200 steps target
              rankKey: 'Q2'),
        ],
      ),
    ];
    setState(() {
      _chartKey.currentState.updateData(nextData);
    });
  }

  @override
  void initState() {
    readEnergy();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          AnimatedCircularChart(
            edgeStyle: SegmentEdgeStyle.round,
            holeLabel: "TODAY",
            labelStyle: TextStyle(fontSize: 25, letterSpacing: 2),
            key: _chartKey,
            duration: Duration(seconds: 1),
            initialChartData: <CircularStackEntry>[
              CircularStackEntry(
                <CircularSegmentEntry>[
                  CircularSegmentEntry(
                    0,
                    Colors.blue,
                  ),
                  CircularSegmentEntry(
                    100,
                    Colors.grey,
                  )
                ],
              ),
            ],
            size: Size(250, 250),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Icon(
                FontAwesomeIcons.fire,
                color: Colors.blue,
              ),
              Icon(
                FontAwesomeIcons.walking,
                color: Colors.green,
              )
            ],
          )
        ],
      ),
    ));
  }
}
